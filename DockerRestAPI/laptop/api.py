
from flask_restful import Resource, Api
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import csv
from io import StringIO

app = flask.Flask(__name__)
api = Api(app)
# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient("dockerrestapi_db_1", 27017)
db = client.tododb

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
        'Yet another laptop!'
            ]
        }

api.add_resource(Laptop, '/')

class listAll(Resource):
    def get(self):
        items = db.tododb.find()
        opentime = []
        closetime = []
        for item in items:
            opentime.append(item["open_time"])
            closetime.append(item["close_time"])
        return {
            'Open_time': opentime,
            'Close_time': closetime
        }
api.add_resource(listAll, '/listAll', '/listAll/json')


class listOpenOnly(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        opentime = []
        result = []
        count = 0
        for item in items:
            opentime.append(item["open_time"])
            count += 1
        opentime.sort()
        if top != None:
            if top <= count:
                for i in range(top):
                    result.append(opentime[i])
                return {
                'Open_time': result
                } 
            else:
                return {
                'Open_time': opentime
                }
        else:   
            return {
            'Open_time': opentime
            }
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')


class listCloseOnly(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        closetime = []
        result = []
        count = 0
        for item in items:
            closetime.append(item["close_time"])
            count += 1
        closetime.sort()
        if top != None:
            if top <= count:
                for i in range(top):
                    result.append(closetime[i])
                return {
                'Close_time': result
                }   
            else:
                return {
                'Close_time': closetime
                }
        else:   
            return {
            'Close_time': closetime
            }
        
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

class listAllcsv(Resource):
    def get(self):
        items = db.tododb.find()
        opentime = " "
        closetime = " "
        for item in items:
            opentime += str(item["open_time"]) + ", "
            closetime += str(item["close_time"]) + ", "
        return opentime + closetime

api.add_resource(listAllcsv, '/listAll/csv')


class listOpenOnlycsv(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        opentime = []
        result = []
        count = 0
        for item in items:
            opentime.append(item["open_time"])
            count += 1
        opentime.sort()
        opentime_str = " "
        if top != None:
            if top <= count:
                for i in range(top):
                    opentime_str += str(opentime[i]) + ', '
                return opentime_str
            else:   
                for item in opentime:
                    opentime_str += str(item) + ", "
                return opentime_str
        else:   
            for item in opentime:
                opentime_str += (item + ", ")
            return opentime_str

api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')

class listCloseOnlycsv(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        closetime = []
        result = []
        count = 0
        for item in items:
            closetime.append(item["close_time"])
            count += 1
        closetime.sort()
        closetime_str = " "
        if top != None:
            if top <= count:
                for i in range(top):
                    closetime_str += str(closetime[i]) + ", "
                return closetime_str
            else:   
                for item in closetime:
                    closetime_str += str(item) + ", "
                return closetime_str
        else:   
            for item in closetime:
                closetime_str += str(item) + ", "
            return closetime_str

api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')





if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
