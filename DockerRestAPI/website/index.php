<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of laptops</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll');
            $obj = json_decode($json);
                $opentime = $obj -> Open_time;
                $closetime = $obj -> Close_time;
            foreach ($opentime as $key) {
                echo "<li>$key</li>";
            }
            foreach ($closetime as $key) {
                echo "<li>$key</li>";
            }

            $json = file_get_contents('http://laptop-service:5000/listAll/json');
            $obj = json_decode($json);
                $opentime = $obj -> Open_time;
                $closetime = $obj -> Close_time;
            foreach ($opentime as $key) {
                echo "<li>$key</li>";
            }
            foreach ($closetime as $key) {
                echo "<li>$key</li>";
            }

            $json = file_get_contents('http://laptop-service:5000/listOpenOnly');
            $obj = json_decode($json);
                $opentime = $obj -> Open_time;
            foreach ($opentime as $key) {
                echo "<li>$key</li>";
            }

            $json = file_get_contents('http://laptop-service:5000/listOpenOnly/json');
            $obj = json_decode($json);
                $opentime = $obj -> Open_time;
            foreach ($opentime as $key) {
                echo "<li>$key</li>";
            }

            $json = file_get_contents('http://laptop-service:5000/listCloseOnly');
            $obj = json_decode($json);
                $closetime = $obj -> Close_time;
            foreach ($closetime as $key) {
                echo "<li>$key</li>";
            }

            $json = file_get_contents('http://laptop-service:5000/listCloseOnly/json');
            $obj = json_decode($json);
                $closetime = $obj -> Close_time;
            foreach ($closetime as $key) {
                echo "<li>$key</li>";
            }

            $listAll_csv = file_get_contents('http://laptop-service:5000/listAll/csv');
                echo $listAll_csv;

            $listopen_csv = file_get_contents('http://laptop-service:5000/listOpenOnly_csv');
                echo $listopen_csv;

            $listclose_csv = file_get_contents('http://laptop-service:5000/listCloseOnly_csv');
                echo $listclose_csv;

            
            ?>
        </ul>
    </body>
</html>
