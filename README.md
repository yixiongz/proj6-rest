-author: Eric Zhou

-contact address email: yixiongz@uoregon.edu

-description: 
            A web based local ACP Brevet Control Times Calculator calculator with a database
It receive the input data from frontend, send the data to backend by AJAX, after solve the data
the result by be sent back by the same way. When click the submit button, data from input will be stored in to, when click the display button, the data which are stored in the database will be displayed on the browser.
            There is also a RESTful service to expose what is stored in MongoDB with several basic APIs.
            By entry specific url, it will display the pertinent content.

            "http://<host:port>/listAll" should return all open and close times in the database
            "http://<host:port>/listOpenOnly" should return open times only
            "http://<host:port>/listCloseOnly" should return close times only
            "http://<host:port>/listAll/csv" should return all open and close times in CSV format
            "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
            "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format
            "http://<host:port>/listAll/json" should return all open and close times in JSON format
            "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
            "http://<host:port>/listCloseOnly/json" should return close times only in JSON format
            "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format
            "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
            "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
            "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

-reference:https://rusa.org/pages/acp-brevet-control-times-calculator
           https://rusa.org/octime_acp.html
